﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySprite : MonoBehaviour {
	EnemyMain enemyMain;

	void Awake () {
		// EnemyMainを検索
		enemyMain = GetComponentInParent<EnemyMain>();
	}

	/* カメラ内にレンダー処理(描画処理)を持つコンポーネントがある場合,
	そのオブジェクトにメッセージを送信*/
	void OnWillRenderObject () {
		// MainCameraに表示されていれば
		if (Camera.current.tag == "MainCamera") {
			// カメラ内に描画された
			enemyMain.cameraEnabled = true;
		}
	}
}
