﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBodyCollider : MonoBehaviour {
	EnemyController enemyCtrl;
	Animator playerAnim;
	int attackHash = 0;

	AudioSource damageFencerSE;
	AudioSource damageSorcererSE;

	void Awake() {
		enemyCtrl = GetComponentInParent<EnemyController> ();
		playerAnim = PlayerController.GetAnimator();
		AudioSource[] audioSource = GetComponents<AudioSource>();
		damageFencerSE = audioSource[0];
		damageSorcererSE = audioSource[1];
	}

	void OnTriggerEnter2D(Collider2D other) {
		// プレイヤーの攻撃がヒットしたら
		if (enemyCtrl.cameraRendered) {
			if (other.tag == "PlayerArm") {
				AnimatorStateInfo stateInfo = playerAnim.GetCurrentAnimatorStateInfo (0);
				// 攻撃アニメーション中は1回だけ判定を行う
				if (attackHash != stateInfo.fullPathHash) {
					// 一回ヒットしたらハッシュ値を保存
					attackHash = stateInfo.fullPathHash;
					enemyCtrl.ActionDamage ();
					damageFencerSE.PlayOneShot (damageFencerSE.clip);
				}
			}
			if(other.tag == "PlayerArmBullet"){
            	Destroy(other.gameObject);
                enemyCtrl.ActionDamage();
				damageSorcererSE.PlayOneShot (damageSorcererSE.clip);
			}
		}
	}
}
