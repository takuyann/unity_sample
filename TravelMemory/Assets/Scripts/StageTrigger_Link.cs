﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StageTrigger_Link : MonoBehaviour {
	// == 外部パラメータ(Inspector表示)
	public string jumpSceneName;
	public float jumpDelayTime = 0.0f;

	// == 内部パラメータ
	Transform playerTrfm;
	PlayerController playerCtrl;

	void Update () {
		playerTrfm = PlayerController.GetTranform();
		playerCtrl = playerTrfm.GetComponent<PlayerController>();
	}

	void OnTriggerEnter2D_PlayerEvent(GameObject go) {
		Jump();
	}

	// ==コード(シーンジャンプの実装)
	public void Jump() {
		// ジャンプ先指定
		if(jumpSceneName == "") {
			jumpSceneName = SceneManager.GetActiveScene().name;
		}
			
		// チェックポイント
		if (StageTrigger_CheckPoint.inputAny) {
			PlayerController.initParam = false;
			PlayerController.checkPointEnabled = true;
			PlayerController.checkPointSceneName = jumpSceneName;
			PlayerController.checkPointHp = PlayerController.nowHp;
			PlayerController.checkPointHpS = PlayerController.nowHpS;
		} else {
			PlayerController.initParam = false;
			PlayerController.checkPointEnabled = true;
			PlayerController.checkPointSceneName = jumpSceneName;
			PlayerController.checkPointHp = PlayerController.nowHp;
			PlayerController.checkPointHpS = PlayerController.nowHpS;
		}

		playerCtrl.ActionMove (0.0f);
		playerCtrl.activeSts = false;

		Invoke ("JumpWork", jumpDelayTime);
	}

	void JumpWork() {
		playerCtrl.activeSts = true;
		SceneManager.LoadScene(jumpSceneName);
	}
}
