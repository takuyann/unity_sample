﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement; 

public class PlayerController : BaseCharacterController {
    // == 外部パラメータ初期化(inspector表示)
    public static readonly float initHpMax = 20.0f;
	public static readonly float initHpMaxS = 15.0f;
    [Range(0.1f, 100.0f)]public float initSpeed = 10.0f;
	AudioSource jumpSE;
	AudioSource attackFencerSE;
	AudioSource attackSorcererSE;
	AudioSource damageSE;

	// == セーブパラメータ
	public static float nowHpMax = 0;
	public static float nowHp = 0;
	public static int score = 0;

	public static float nowHpMaxS = 0;
	public static float nowHpS = 0;

	public static float tmpHp = 0;
	public static float tmpHpS = 0;

	public static bool checkPointEnabled = false;
	public static string checkPointSceneName = "";
	public static float checkPointHp = 0;
	public static float checkPointHpS = 0;

	// 内部からの処理操作用パラメータ
	public static bool initTmp = true;

	// 外部からの処理操作用パラメータ
	public static bool initParam = true;
	public static bool gameOverNow = false;

	// 攻撃力アップ中判定パラメータ
	public bool attackUp = false;

	// 外部パラメータ
	// アニメーションのハッシュ名(チェックしたいステート名をハッシュ値にする)
	public readonly static int ANISTS_Idle = Animator.StringToHash("Base Layer.Player_Idle");
	public readonly static int ANISTS_Walk = Animator.StringToHash("Base Layer.Player_Walk");
	public readonly static int ANISTS_Run = Animator.StringToHash("Base Layer.Player_Run");
	public readonly static int ANISTS_Jump = Animator.StringToHash("Base Layer.Player_Jump");
	public readonly static int ANISTS_ATTACK_A = Animator.StringToHash ("Base Layer.Player_ATK_A");
	public readonly static int ANISTS_ATTACK_B = Animator.StringToHash ("Base Layer.Player_ATK_B");
    public readonly static int ANISTS_DMG_A = Animator.StringToHash("Base Layer.Player_DMG_A");
	public readonly static int ANISTS_DEAD = Animator.StringToHash ("Base Layer.Player_Dead");

    // == キャッシュ
    LineRenderer hudHpBar;
	LineRenderer hudHpBarS;
    TextMesh hudScore;

    // == 内部パラメータ
    int jumpCount = 0;

	// volatile…(変数の代入処理の省略化といった最適化をコンパイラが行われないようにする)
	volatile bool atkInputEnable = false;	
	volatile bool atkInputNow = false;

    bool breakEnabled = true;
    float groundFriction = 0.0f;

	// サポート関数
	public static GameObject GetGameObject() {
		return GameObject.FindGameObjectWithTag("Player");
	}

	public static Transform GetTranform() {
		return GameObject.FindGameObjectWithTag("Player").transform;
	}

	public static PlayerController GetController() {
		return GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerController> ();
	}

	public static Animator GetAnimator() {
		return GameObject.FindGameObjectWithTag ("Player").GetComponent<Animator> ();
	}

    // == コード(パラメータをプレイヤー用に初期化)
    protected override void Awake() {
        base.Awake();
	
        // キャッシュ
        hudHpBar = GameObject.Find("HUD_HPBar").GetComponent<LineRenderer>();
		hudHpBarS = GameObject.Find("HUD_HPBarS").GetComponent<LineRenderer>();
        hudScore = GameObject.Find("HUD_Score").GetComponent<TextMesh>();

        // パラメータ初期化
        //speed = initSpeed;
        if (initParam)
        {
			if (PlayerChange.playerChange) {	
				SetHP(initHpMax, initHpMax);
				SetHPS (0, initHpMaxS);
			} else {
				SetHPS(initHpMaxS, initHpMaxS);
				SetHP (0, initHpMax);
			}
		}

		if (!initParam) {
			if (StageTrigger_CheckPoint.inputAny) {
				if (initTmp) {
					tmpHp = checkPointHp;
					tmpHpS = checkPointHpS;
					if (tmpHp == 0) {
						tmpHp = hp;
					} else if (tmpHpS == 0) {
						tmpHpS = hpS;
					}
					initTmp = false;
				}
			} else {
				if (initTmp) {
					tmpHp = checkPointHp;
					tmpHpS = checkPointHpS;
					if (tmpHp == 0) {
						tmpHp = hp;
					} else if (tmpHpS == 0) {
						tmpHpS = hpS;
					}
					initTmp = false;
				}
			}
			if (PlayerChange.playerChange) {
				SetHP (tmpHp, initHpMax);
				SetHPS (0, initHpMaxS);
			} else {
				SetHP (0, initHpMax);
				SetHPS (tmpHpS, initHpMaxS);
			}
		}
			
		Camera.main.transform.position = new Vector3 (
			transform.position.x , transform.position.y , Camera.main.transform.position.z);

		AudioSource[] audioSource = GetComponents<AudioSource>();
		jumpSE = audioSource[0];
		attackFencerSE = audioSource[1];
		attackSorcererSE = audioSource[2];
		damageSE = audioSource[3];
    }
		
    protected override void Update() {
        base.Update();

        // ステータス表示
        // HPと最大HPの割合をHPバーの長さに変換
        hudHpBar.SetPosition(1, new Vector3(5.0f * (hp / hpMax), 0.0f, 0.0f));
		hudHpBarS.SetPosition(1, new Vector3(5.0f * (hpS / hpMaxS), 0.0f, 0.0f));
        hudScore.text = string.Format("Score {0}", score);
    }

	// ジャンプ後の着地判定,キャラの移動方向に合わせた反転処理,キャラの移動停止処理
    protected override void FixedUpdateCharacter() {
		// 	現在のステート名
		AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(0);

        // 着地チェック
        if (jumped) {
			// 前フレームが接地せず,現在のフレームが接地していれば(1秒後に着地と判定)
            if ((grounded && !groundedPrev) ||
                (grounded && Time.fixedTime > jumpStartTime + 1.0f)) {
                animator.SetTrigger("Idle");
                jumped = false;
                jumpCount = 0;
            }
        }
        if (!jumped) {
            jumpCount = 0;
        }

		// 攻撃中であるか?(攻撃中のときは移動できないようにする)
		if (stateInfo.fullPathHash == ANISTS_ATTACK_A || stateInfo.fullPathHash == ANISTS_ATTACK_B) {
			// 移動停止
			speedVx = 0;

		}

        // キャラの方向
        transform.localScale = new Vector3(
            basScaleX * dir, transform.localScale.y, transform.localScale.z);

        // ジャンプ中の横移動減速
        if (jumped && !grounded) {
            if (breakEnabled) {	// 移動操作をしていなければ
                breakEnabled = false;
                speedVx *= 0.9f; // フレームごとに10%ずつ減速
            }
        }

        // 移動停止(減速)処理
        if (breakEnabled) {
            speedVx *= groundFriction;
        }
    }


	// 入力判定許可(アニメーションイベント用コード)
	public void EnableAttackInput(){
		atkInputEnable = true;	// 入力判定許可を有効にする
	}

	// 次の攻撃アニメーション遷移
	public void SetNextAttack(string name) {
		if (atkInputNow == true) {	// コンボが成立していれば
			atkInputNow = false;
			animator.Play (name);	// 次のアニメーションを強制的に再生
		}
	}

    // == コード
    public override void ActionMove(float n) {
        if (!activeSts) {	// プレイヤーが死んでいれば
            return;
        }

        // 初期化
        float dirOld = dir;
        breakEnabled = false;

        /* アニメーション指定(PlayerMainクラスのUpdate関数で取得した入力値に合わせて
		 PlayerAnimControllerのMovSpeedを絶対値でセットする(0.3未満=歩き,0.5より大きい=走り*/
        float moveSpeed = Mathf.Clamp(Mathf.Abs(n), -1.0f, +1.0f);
        animator.SetFloat("MovSpeed",moveSpeed); 

        // 移動チェック(移動距離speedVx,キャラの基本速度initSpeed,キャラの向きdir)
        if (n != 0.0f) {
            // 移動
            dir = Mathf.Sign(n);
            moveSpeed = (moveSpeed < 0.5f) ? (moveSpeed * (1.0f / 0.5f)) : 1.0f;
            speedVx = initSpeed * moveSpeed * dir;
        } else {
            // 移動停止
            breakEnabled = true;
        }

        // その場振り向きチェック
        if (dirOld != dir) {
            breakEnabled = true;
        }

    }

    public void ActionJump() {
        switch (jumpCount) {
            case 0 :
                if (grounded) {
                    animator.SetTrigger("Jump");
                    GetComponent<Rigidbody2D>().velocity = Vector2.up * 26.0f;
					jumpSE.PlayOneShot (jumpSE.clip);
                    jumpStartTime = Time.fixedTime;
                    jumped = true;
                    jumpCount ++;
                }
				break;
            case 1 :
                if (!grounded) {
                    animator.Play("Player_Jump",0,0.0f);  // 自分自身のステートに遷移
                    GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x,20.0f);
					jumpSE.PlayOneShot (jumpSE.clip);
                    jumped = true;
                    jumpCount ++;
                }
                break;
        }
    }

	public void ActionJumpS() {
		if (grounded) {
			animator.SetTrigger("Jump");
			GetComponent<Rigidbody2D>().velocity = Vector2.up * 25.0f;
			jumpSE.PlayOneShot (jumpSE.clip);
			jumpStartTime = Time.fixedTime;
			jumped = true;
			jumpCount ++;
		}
	}

	public void ActionAttack() {
		AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(0);	// 攻撃の切り替え判定
		if (stateInfo.fullPathHash == ANISTS_Idle || stateInfo.fullPathHash == ANISTS_Walk ||
		    stateInfo.fullPathHash == ANISTS_Run || stateInfo.fullPathHash ==ANISTS_Jump) { // 現在実行されているアニメーションのステートが攻撃A以外ならBを実行
			animator.SetTrigger ("Attack_A");
		} else {
			if (atkInputEnable) {	// 入力判定許可が有効であれば
				atkInputEnable = false;
				atkInputNow = true;	// コンボが成立した
			}
		}
	}

	public void ActionDamage(float damage) {
		if (!activeSts) {
			return;
		}

		animator.SetTrigger ("DMG_A");
		damageSE.PlayOneShot (damageSE.clip);
		speedVx = 0;
		GetComponent<Rigidbody2D> ().gravityScale = gravityScale;

		if (SetHP(hp - damage,hpMax)) {
			Dead();	// 死亡
		}
	}

	public void ActionDamageS(float damage) {
		if (!activeSts) {
			return;
		}

		animator.SetTrigger ("DMG_A");
		damageSE.PlayOneShot (damageSE.clip);
		speedVx = 0;
		GetComponent<Rigidbody2D> ().gravityScale = gravityScale;

		if (SetHPS(hpS - damage,hpMaxS)) {
			Dead();	// 死亡
		}
	}

	// == コード(その他)
	public override void Dead() {
		// 死亡処理をしてもいいか
		AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo (0);
		if (!activeSts || stateInfo.fullPathHash == ANISTS_DEAD) {
			return;
		}

		base.Dead ();

		SetHP (0, hpMax);
		SetHPS (0, hpMaxS);
		Invoke ("GameOver", 3.0f);

        // GAME OVERと表示させる
		gameOverNow = true;
		GameObject.Find("HUD_GameOver").GetComponent<MeshRenderer>().enabled = true;
        GameObject.Find("HUD_GameOverShadow").GetComponent<MeshRenderer>().enabled = true;

		if (PlayerChange.playerChange) {
			GameObject.Find("/ChoosePlayers/Player/PlayerSprite").SetActive(false);
		} else {
			GameObject.Find("/ChoosePlayers/PlayerS/PlayerSprite").SetActive(false);
		}
	}

	public void GameOver() {
		PlayerController.score = 0;
		//SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}

	public void ActionAttackFencerSE() {
		attackFencerSE.PlayOneShot (attackFencerSE.clip);
	}

	public void ActionAttackSorcererSE() {
		attackSorcererSE.PlayOneShot (attackSorcererSE.clip);
	}

	public override bool SetHP(float _hp,float _hpMax) {
		if (_hp > _hpMax) {
			_hp = _hpMax;
		}

		nowHp = _hp;
		nowHpMax = _hpMax;
		return base.SetHP (_hp, _hpMax);
	}

	public override bool SetHPS(float _hpS,float _hpMaxS) {
		if (_hpS > _hpMaxS) {
			_hpS = _hpMaxS;
		}

		nowHpS = _hpS;
		nowHpMaxS = _hpMaxS;
		return base.SetHPS (_hpS, _hpMaxS);
	}
}
