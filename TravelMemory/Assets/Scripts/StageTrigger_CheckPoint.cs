﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StageTrigger_CheckPoint : MonoBehaviour {
	public CameraFollow.Param cameraParam;
	public static bool inputAny = false;

	void Update(){
		// EventTriggerにヒットしたときに、いずれかのキーが入力されていたら
		if (Input.anyKey && PlayerBodyCollider.triggerInput) {
			inputAny = true;
		} 
	}
	/*チェックポイントのラベル名と、チェックポイントに移動したときに
	 セットするカメラ情報*/
	void OnTriggerEnter2D_PlayerEvent(GameObject go) {
		if (inputAny) {
			PlayerController.initParam = false;
			PlayerController.checkPointEnabled = true;
			PlayerController.checkPointSceneName = SceneManager.GetActiveScene ().name;
			PlayerController.checkPointHp = PlayerController.nowHp;
			PlayerController.checkPointHpS = PlayerController.nowHpS;
			Camera.main.GetComponent<CameraFollow> ().SetCamera (cameraParam);
		} else {
			PlayerController.initParam = false;
			PlayerController.checkPointEnabled = true;
			PlayerController.checkPointSceneName = SceneManager.GetActiveScene ().name;
			PlayerController.checkPointHp = PlayerController.nowHp;
			PlayerController.checkPointHpS = PlayerController.nowHpS;
			Camera.main.GetComponent<CameraFollow> ().SetCamera (cameraParam);
		}
	}
}
