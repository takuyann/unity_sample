﻿using UnityEngine;
using System.Collections;

public class BaseCharacterController : MonoBehaviour {
    // == 外部パラメータ(Inspector表示)
    public Vector2 velocityMin = new Vector2(-100.0f, -100.0f);
    public Vector2 velocityMax = new Vector2(+100.0f, +50.0f);
	public bool superArmor = false;
	public GameObject[] fireObjectList;

    // == 外部パラメータ([System.NonSerialized]はinspectorで編集されたくない)
    [System.NonSerialized]public float hpMax = 20.0f;
    [System.NonSerialized]public float hp = 20.0f;
	[System.NonSerialized]public float hpMaxS = 15.0f;
	[System.NonSerialized]public float hpS= 15.0f;
    [System.NonSerialized]public float dir = 1.0f;
    [System.NonSerialized]public float speed = 6.0f;
    [System.NonSerialized]public float basScaleX = 1.0f;
    [System.NonSerialized]public bool activeSts = false;
    [System.NonSerialized]public bool jumped = false;
    [System.NonSerialized]public bool grounded = false;
    [System.NonSerialized]public bool groundedPrev = false;

    // == キャッシュ 
    [System.NonSerialized]public Animator animator;
    protected Transform groundCheck_L;
    protected Transform groundCheck_C;
    protected Transform groundCheck_R;

    // == 内部パラメータ
    protected float speedVx = 0.0f;
    protected float speedVxAddPower = 0.0f;
    protected float gravityScale = 10.0f;
    protected float jumpStartTime = 0.0f;

    protected GameObject groundCheck_OnRoadObject;
    protected GameObject groundCheck_OnMoveObject;
    protected GameObject groundCheck_OnEnemyObject;

    // == コード (キャッシュとパラメータの初期化)
    protected virtual void Awake() {
        animator = GetComponent <Animator>();
        groundCheck_L = transform.Find("GroundCheck_L"); //GameObject.Findだと検索処理が重くなる(子オブジェクトの検索を行う)                                                        
        groundCheck_C = transform.Find("GroundCheck_C"); 
        groundCheck_R = transform.Find("GroundCheck_R");

        dir = (transform.localScale.x > 0.0f) ? 1 : -1;
        basScaleX = transform.localScale.x * dir;
        transform.localScale = new Vector3(
            basScaleX, transform.localScale.y, transform.localScale.z);

        activeSts = true;
        gravityScale = GetComponent<Rigidbody2D>().gravityScale;
    }

    // 拡張する可能性(オーバーライドできる仮想関数としての宣言)
	protected virtual void Start () {
	}
	
	protected virtual void Update () {
	}

    //キャラの基本処理(物理挙動の更新の直前に呼ばれる)
    protected virtual void FixedUpdate() {
        // 落下チェック
        if (transform.position.y < -20.0f){ // y座標で-30より下に落下していれば
			Dead();    // 死亡
        }

        //  地面との接地判定処理
        groundedPrev = grounded;
        grounded = false;
        groundCheck_OnRoadObject = null;
        groundCheck_OnMoveObject = null;
        groundCheck_OnEnemyObject = null;

        Collider2D[][] groundCheckCollider = new Collider2D[3][];
        groundCheckCollider[0] = Physics2D.OverlapPointAll(groundCheck_L.position); //ヒットしているすべてのオブジェクトを返す
        groundCheckCollider[1] = Physics2D.OverlapPointAll(groundCheck_C.position);
        groundCheckCollider[2] = Physics2D.OverlapPointAll(groundCheck_R.position);

        foreach (Collider2D[] groundCheckList in groundCheckCollider){
            foreach (Collider2D groundCheck in groundCheckList){
                if (groundCheck != null){
                    if (!groundCheck.isTrigger) { //トリガーでないコライダー(キャラが接地したもの)を取得したら
                        grounded = true;
                        if (groundCheck.tag == "Road") { // カメラ処理やギミック処理で利用
                            groundCheck_OnRoadObject = groundCheck.gameObject;
                        } else if (groundCheck.tag == "MoveObject") {
                            groundCheck_OnMoveObject = groundCheck.gameObject;
                        } else if (groundCheck.tag == "EnemyObject") {
                            groundCheck_OnEnemyObject = groundCheck.gameObject;
                        }
                    }
                }
            }
        }
        // キャラクター個別の処理
		FixedUpdateCharacter(); //移動やジャンプ処理

		// 乗り物チェック
		/* 接地ポイントがMoveObjectタグのゲームオブジェクトを検出したら、そのゲームオブジェクトを
		検出したら、そのゲームオブジェクトからRigidbody2Dコンポーネントを検索し、velocity値をキャラのvelocity値に
		コピーして、移動する物体の速度をキャラにも反映させる*/
		if (grounded) {
			speedVxAddPower = 0.0f;
			if (groundCheck_OnMoveObject != null) {
				Rigidbody2D rb2D = 
				groundCheck_OnMoveObject.GetComponentInParent<Rigidbody2D> ();
				speedVxAddPower = rb2D.velocity.x;
			}
		}

        // 移動計算
		GetComponent<Rigidbody2D>().velocity = new Vector2 (speedVx + speedVxAddPower, GetComponent<Rigidbody2D>().velocity.y);

        // Velocityの値をチェック
        float vx = Mathf.Clamp(GetComponent<Rigidbody2D>().velocity.x,velocityMin.x,velocityMax.x); //値の範囲を超えないように制限している
        float vy = Mathf.Clamp(GetComponent<Rigidbody2D>().velocity.y,velocityMin.y,velocityMax.y); //外部パラメータで宣言した値で設定
        GetComponent<Rigidbody2D>().velocity = new Vector2(vx,vy);
    }

    protected virtual void FixedUpdateCharacter() {

    }

	// アニメーションイベントコード
	public void EnableSuperArmor() {
		superArmor = true;
	}

	public void DisableSuperArmor() {
		superArmor = false;
	}

    // == 基本アクションのコード(敵キャラにのみ使用)
    public virtual void ActionMove(float n) {
        if (n != 0.0f){ //移動していれば
            dir = Mathf.Sign(n);
            speedVx = speed * n;
            animator.SetTrigger("Run");
        } else { //停止していれば              
            speedVx = 0;
            animator.SetTrigger("Idle");
        }
    }

	// 攻撃アニメーションから呼び出される
	// fireObjectListに登録されているゲームオブジェクトをInstantiate関数で複製
	public void ActionFire() {
		Transform goFire = transform.Find("Muzzle");
		foreach (GameObject fireObject in fireObjectList) {
			GameObject go = Instantiate(fireObject,goFire.position,Quaternion.identity) as GameObject;
			/* 誰が発射したものかを保持するためにInstantiate関数で受け取ったインスタンスから
			 FireBulletコンポーネントを取得しownwerプロパティに親である自分のtransformを渡す*/
			go.GetComponent<FireBullet>().ownwer = transform;
		}
	}

    public void ActionFireS(){
        Transform goFire = transform.Find("MuzzleS");
        foreach (GameObject fireObject in fireObjectList) {
            GameObject go = Instantiate(fireObject, goFire.position, Quaternion.identity) as GameObject;
            /* 誰が発射したものかを保持するためにInstantiate関数で受け取ったインスタンスから
             FireBulletSコンポーネントを取得しownwerSプロパティに親である自分のtransformを渡す*/
            go.GetComponent<FireBulletS>().ownwerS = transform;
        }
    }

	// キャラが指定したゲームオブジェクトの方向を向く
	public bool ActionLookup(GameObject go,float near) {
		if (Vector3.Distance (transform.position, go.transform.position) > near) {
			dir = (transform.position.x < go.transform.position.x) ? +1 : -1;
			return true;
		}
		return false;
	}

	// 指定したゲームオブジェクトに近づく
	public bool ActionMoveToNear(GameObject go, float near) {
		if (Vector3.Distance (transform.position, go.transform.position) > near) {
			ActionMove((transform.position.x < go.transform.position.x) ? +1.0f : -1.0f);
			return true;
		}
		return false;
	}

	// 指定したゲームオブジェクトから離れる
	public bool ActionMoveToFar(GameObject go, float far) {
		if (Vector3.Distance (transform.position, go.transform.position) < far) {
			ActionMove((transform.position.x > go.transform.position.x) ? +1.0f : -1.0f);
			return true;
		}
		return false;
	}

    // == HP処理やゲームオーバー(穴に落ちたら偽(チェックポイント再開)、敵に倒されたら真(メニューに戻る)
    public virtual void Dead() {
        if (!activeSts) {
            return;
        }
        activeSts = false; // キャラを行動不能にする
        animator.SetTrigger("Dead"); // 死亡アニメーションを実行
    }
		
    public virtual bool SetHP(float _hp, float _hpMax) {
        hp = _hp;　// キャラのHP
        hpMax = _hpMax; //最大HP
        return (hp <= 0); //設定したHPが0以下だとfalse
    }

	public virtual bool SetHPS(float _hpS, float _hpMaxS) {
		hpS = _hpS;　// キャラのHP
		hpMaxS = _hpMaxS; //最大HP
		return (hpS <= 0); //設定したHPが0以下だとfalse
	}
}
