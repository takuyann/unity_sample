﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMain_A : EnemyMain {
	// 外部パラメータ
	public int aiIfRUNTOPLAYER = 20;
	public int aiIfJUMPTOPLAYER = 30;
	public int aiIfESCAPE = 10;

	public int damageAttack_A = 1;
	AudioSource attackSE;

	public override void Start () {
		AudioSource audioSource = GetComponent<AudioSource>();
		attackSE = audioSource;
	}

	// == コード(AI思考処理)
	public override void FixedUpdateAI() {
		// AIステート
		switch (aiState) {
			// ランダムに行動を選択するステート
			case ENEMYAISTS.ACTIONSELECT :	// 思考の拠点
				//　アクションの選択(各ステートが時間オーバーすると,このステートに戻る)
				int n = SelectRandomAIState();
				if (n < aiIfRUNTOPLAYER) {
					SetAIState(ENEMYAISTS.RUNTOPLAYER,3.0f);
				}else if (n < aiIfRUNTOPLAYER + aiIfJUMPTOPLAYER) {
					SetAIState(ENEMYAISTS.JUMPTOPLAYER,1.0f);
				}else if (n < aiIfRUNTOPLAYER + aiIfJUMPTOPLAYER + aiIfESCAPE) {
					SetAIState(ENEMYAISTS.ESCAPE, Random.Range(2.0f,5.0f));
				}else {
					SetAIState(ENEMYAISTS.WAIT,1.0f + Random.Range(0.0f,1.0f));
				}
				enemyCtrl.ActionMove(0.0f);
				break;

			// 一定時間(止まって)待つ
			case ENEMYAISTS.WAIT :	// 休憩
				// キャラがプレイヤーの方を向く
				enemyCtrl.ActionLookup(player,0.1f);
				enemyCtrl.ActionMove(0.0f);
				break;

			// 走ってプレイヤーに近づく
			case ENEMYAISTS.RUNTOPLAYER :	// 近寄る
				// プレイヤーよりも低い位置にいる場合
				if (GetDistanePlayerY() > 3.0f) {
					// ジャンプで近寄る
					SetAIState(ENEMYAISTS.JUMPTOPLAYER,1.0f);
				}
				// プレイヤーの一定範囲内に近づいた場合
				if (!enemyCtrl.ActionMoveToNear(player,3.0f)) {
					Attack_A();
				}
				break;

			// ジャンプしてプレイヤーに近づく
			case ENEMYAISTS.JUMPTOPLAYER :	// ジャンプで近寄る
				// プレイヤーに攻撃がヒットする距離か,すでに移動やジャンプを行っていないか
				if (GetDistanePlayer() < 2.0f && IsChangeDistanePlayer(0.5f)) {
					Attack_A();
					break;
				}
				enemyCtrl.ActionJump();
				// プレイヤー方向に斜めにジャンプ
				enemyCtrl.ActionMoveToNear(player,0.1f);
				// ジャンプ中は移動処理以外は行わないように,思考を止めている
				SetAIState(ENEMYAISTS.FREEZ, 0.5f);
				break;

			// プレイヤーから一定の距離以上に離れて逃げる
			case ENEMYAISTS.ESCAPE :	// 遠ざかる
				if(!enemyCtrl.ActionMoveToFar(player,7.0f)) {
					SetAIState(ENEMYAISTS.ACTIONSELECT,1.0f);
				}
				break;
		}
	}

	// 攻撃アクション
	void Attack_A() {
		enemyCtrl.ActionLookup(player, 0.1f);
		enemyCtrl.ActionMove (0.0f);
		enemyCtrl.ActionAttack ("Attack_A", damageAttack_A);
		SetAIState(ENEMYAISTS.WAIT, 2.0f);
	}

	public void EnemyA_AttackSE() {
		attackSE.PlayOneShot (attackSE.clip);
	}
}
