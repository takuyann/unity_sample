﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBulletS : MonoBehaviour
{
	// 外部パラメータ(inspector表示)
	public FIREBULLET fireType = FIREBULLET.HOMING;	// 弾の発射タイプ

	public float attackDamage = 1;
	public bool penetration = false;	// オンで障害物を貫通

	public float lifeTime = 3.0f;	// 弾の生存時間
	public float speedV = 10.0f;	// 速度
	public float speedA = 0.0f;	    // 加速度
	public float angle = 0.0f;	    // 角度(発射時に撃つ方向)

	public float homingTime = 0.0f;	// ホーミングする時間

	public Vector3 bulletScaleV = Vector3.zero;	 //弾の拡大速度
	public Vector3 bulletScaleA = Vector3.zero;	 // 弾の拡大加速度

	public Sprite hiteSprite;	// ヒット時のエフェクトスプライト指定
	public Vector3 hitEffectScale = Vector3.one;
	public float rotateVt = 360.0f;

	// == 外部パラメータ
	[System.NonSerialized]
	public Transform ownwerS;
	[System.NonSerialized]
	public GameObject targetObjectS;
	[System.NonSerialized]
	public bool attackEnabled;

	// == 内部パラメータ
	protected float distanceToEnemy = 0.0f;
	float fireTime;
	Vector3 posTargetS;
	Quaternion homingRotateS;
	float speed;

	// ==　コード
	void Start()
	{
		if (!ownwerS)
		{
			return;
		}

		// 初期化
		targetObjectS = EnemyController.GetGameObjectEnemy();
		// (GetDistanePlayer)敵キャラとプレイヤーとの距離を返す
		if (!targetObjectS) {
			fireType = FIREBULLET.ANGLE;
		} else {
			distanceToEnemy = Vector3.Distance (ownwerS.transform.position, targetObjectS.transform.position);
		}
		if (targetObjectS && distanceToEnemy < 6.0f){
			posTargetS = targetObjectS.transform.position + new Vector3(0.0f, 1.0f, 0.0f);
		}
		else {
			fireType = FIREBULLET.ANGLE;
		}

		switch (fireType)
		{
		case FIREBULLET.ANGLE:
			speed = (ownwerS.localScale.x < 0.0f) ? -speedV : +speedV;
			break;

		case FIREBULLET.HOMING:
			speed = speedV;
			homingRotateS = Quaternion.LookRotation(posTargetS - transform.position);
			break;

		}
		fireTime = Time.fixedTime;
		attackEnabled = true;
		Destroy(this.gameObject, lifeTime);
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		// 投げたキャラ自身にヒットした
		if (!ownwerS)
		{
			return;
		}

		if ((other.isTrigger ||
			(other.tag == "Player" && other.tag == "PlayerBody") ||
			(other.tag == "Player" && other.tag == "PlayerArm") ||
			(other.tag == "Player" && other.tag == "PlayerArmBullet") ||
			(other.tag == "Enemy" && other.tag == "EnemyBody") ||
			(other.tag == "Enemy" && other.tag == "EnemyArm") ||
			(other.tag == "Enemy" && other.tag == "EnemyArmBullet")))
		{
			return;
		}

		// 壁あたりをチェック(障害物にヒットしたら消す)
		if (!penetration)
		{
			GetComponent<SpriteRenderer>().sprite = hiteSprite;
			GetComponent<SpriteRenderer>().color = new Color(1.0f, 1.0f, 1.0f, 0.5f);
			transform.localScale = hitEffectScale;
			Destroy(this.gameObject, 0.1f);
		}
	}

	void Update()
	{
		// スプライト画像の回転処理
		//transform.Rotate(0.0f, 0.0f, Time.deltaTime * rotateVt);
	}

	void FixedUpdate()
	{
		// ターゲット設定
		bool homing = ((Time.fixedTime - fireTime) < homingTime);
		if (homing)
		{
			posTargetS = targetObjectS.transform.position + new Vector3(0.0f, 1.0f, 0.0f);
		}

		// ホーミング処理
		switch (fireType)
		{
		case FIREBULLET.ANGLE: // 指定した角度に発射
			GetComponent<Rigidbody2D>().velocity = Quaternion.Euler(0.0f, 0.0f, angle) * new Vector3(speed, 0.0f, 0.0f);
			break;

			// ターゲットのゲームオブジェクトの方向に移動させる
		case FIREBULLET.HOMING:	// 完璧にホーミング
			{
				if (homing)
				{
					// (Quaternion.LookRotation)指定したVector3の座標に対する角度の計算 
					homingRotateS = Quaternion.LookRotation(posTargetS - transform.position);
				}
				// (Vector3.forward)前方向のベクトル
				Vector3 vecMove = (homingRotateS * Vector3.forward) * speed;
				GetComponent<Rigidbody2D>().velocity = Quaternion.Euler(0.0f, 0.0f, angle) * vecMove;
			}
			break;
		}

		// 加速度計算
		speed += speedA * Time.fixedDeltaTime;

		// スケール計算
		transform.localScale += bulletScaleV;
		bulletScaleV += bulletScaleA * Time.fixedDeltaTime;
		if (transform.localScale.x < 0.0f || transform.localScale.y < 0.0f || transform.localScale.z < 0.0f)
		{
			Destroy(this.gameObject);
		}
	}
}
